#!/usr/bin/python

import os, fontforge, re

EXTS = [".woff", "woff2", ".ttf", ".otf", ".svg", ".eot"]

def cssGenerator(name, fullname):
    
    cssFile = name + "/" + name + ".css"
    template = "@font-face {\
            \n\tfont-family: '" + fullname + "';\
            \n\tsrc: url('" + name + ".eot');\
            \n\tsrc: url('" + name + ".eot?#iefix') format('embedded-opentype'),\
            \n\turl('" + name + ".woff') format('woff'),\
            \n\turl('" + name + ".woff2') format('woff2'),\
            \n\turl('" + name + ".ttf') format('truetype'),\
            \n\turl('" + name + ".svg#ywftsvg') format('svg');\
            \n\tfont-style: normal;\
            \n\tfont-weight: normal;\
            \n}\n\n"
    open(cssFile, "a").writelines(template)


def fontGenerator(filename):
    #exts = ["woff", "ttf", "otf", "svg", "eot"]
    name = os.path.splitext(filename)[0]
    
    if not os.path.exists(name):
        os.makedirs(name)
    
    font = fontforge.open(filename)
    fullname = font.fullname
    cssGenerator(name, fullname)
    
    for ext in EXTS:
        f = name + "/" + name + ext
        font.generate(f)


if __name__ == "__main__":
    args = os.sys.argv
        
    if args[1] == "-b":
        path = os.getcwd()
        fileList = os.listdir(path)
                
        for filename in fileList:
            name, ext = os.path.splitext(filename)
            if ext in EXTS:
                fontGenerator(filename)
    else:
        filename = args[1]
        fontGenerator(filename)
