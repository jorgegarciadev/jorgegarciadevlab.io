#include <avr/sleep.h>

#define adc_disable() (ADCSRA &= ~(1<<ADEN))

void bottomRow() {
  // LED's 1 3 5 6 12 16
  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
  digitalWrite(0, HIGH); // LED 1
  digitalWrite(0, LOW);
  pinMode(0, INPUT);
  pinMode(2, OUTPUT);
  digitalWrite(1, HIGH); // LED 3
  digitalWrite(1, LOW);
  pinMode(1, INPUT);
  pinMode(3, OUTPUT);
  digitalWrite(2, HIGH); // LED 5
  digitalWrite(2, LOW);
  digitalWrite(3, HIGH); // LED 6
  pinMode(2, INPUT);
  pinMode(0, OUTPUT);
  digitalWrite(3, LOW); // LED 16
  pinMode(0, INPUT);
  pinMode(1, OUTPUT);
  digitalWrite(3, HIGH); // LED 12
  digitalWrite(3, LOW);
  pinMode(3, INPUT);
  pinMode(1, INPUT);
}

void secondRow() {
  // LED 10 13 14 17
  pinMode(0, OUTPUT);
  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH); // LED 10
  pinMode(0, INPUT);
  pinMode(4, OUTPUT); // LED 13
  digitalWrite(2, LOW);
  digitalWrite(4, HIGH); // LED 14
  digitalWrite(4, LOW);
  pinMode(2, INPUT);
  pinMode(1, OUTPUT);
  digitalWrite(1, HIGH); // LED 17
  digitalWrite(1, LOW);
  pinMode(4, INPUT);
  pinMode(1, INPUT);
}

void thirdRow() {
  // LED 2 11 15 18
  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
  digitalWrite(1, HIGH); // LED 2
  digitalWrite(1, LOW);
  pinMode(1, INPUT);
  pinMode(3, OUTPUT);
  digitalWrite(0, HIGH); // LED 15
  digitalWrite(0, LOW);
  pinMode(0, INPUT);
  pinMode(1, OUTPUT);
  digitalWrite(1, HIGH); // LED 11
  digitalWrite(1, LOW);
  pinMode(3, INPUT);
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);
  digitalWrite(4, LOW);
  pinMode(1, INPUT);
  pinMode(4, INPUT);
}

void topRow() {
  // LEDs 4 7 8 9
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH); // LED 4
  digitalWrite(2, LOW);
  pinMode(1, INPUT);
  pinMode(0, OUTPUT);
  digitalWrite(0, HIGH); // LED 9
  digitalWrite(0, LOW);
  pinMode(0, INPUT);
  pinMode(2, INPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  digitalWrite(3, HIGH); // LED 7
  digitalWrite(3, LOW);
  digitalWrite(4, HIGH); // LED 8
  digitalWrite(4, LOW);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
} 

void stars() {
  // LED 19 and 20
  pinMode(0, OUTPUT);
  pinMode(4, OUTPUT);
  digitalWrite(0, HIGH);
  digitalWrite(0, LOW);
  digitalWrite(4, HIGH);
  digitalWrite(4, LOW);
  digitalWrite(0, HIGH);
  digitalWrite(0, LOW);
  digitalWrite(4, HIGH);
  digitalWrite(4, LOW);
  pinMode(0, INPUT);
  pinMode(4, INPUT);
}

void upModeOne() {
  unsigned long timeTracker=millis();
  while((millis()-timeTracker)<250) {
    bottomRow();
  }
  delay(150);
  timeTracker=millis();
  while((millis()-timeTracker)<250) {
    secondRow();
  }
  delay(150);
  timeTracker=millis();
  while((millis()-timeTracker)<250) {
    thirdRow();
  }
  delay(150);
  timeTracker=millis();
  while((millis()-timeTracker)<250) {
    topRow();
  }
  delay(150);
  timeTracker=millis();
  while((millis()-timeTracker)<250) {
    stars();
  }
}

void upModeTwo() {
  unsigned long timeTracker=millis();
  while((millis()-timeTracker)<400) {
    bottomRow();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    bottomRow();
    secondRow();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    bottomRow();
    secondRow();
    thirdRow();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    bottomRow();
    secondRow();
    thirdRow();
    topRow();
  }
  for (int j=0; j<8; j++) {
    timeTracker=millis();
    while((millis()-timeTracker)<100) {
      bottomRow();
      secondRow();
      thirdRow();
      topRow();
      stars();
    }
    timeTracker=millis();
    while((millis()-timeTracker)<100) {
      bottomRow();
      secondRow();
      thirdRow();
      topRow();
    }
  }
}

void upModeThree() {
  unsigned long timeTracker=millis();
  while((millis()-timeTracker)<400) {
    bottomRow();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    bottomRow();
    secondRow();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    bottomRow();
    secondRow();
    thirdRow();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    bottomRow();
    secondRow();
    thirdRow();
    topRow();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<600) {
    bottomRow();
    secondRow();
    thirdRow();
    topRow();
    stars();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    secondRow();
    thirdRow();
    topRow();
    stars();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    thirdRow();
    topRow();
    stars();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    topRow();
    stars();
  }
  timeTracker=millis();
  while((millis()-timeTracker)<400) {
    stars();
  }
}

void upDownModeOne() {
  unsigned long timeTracker=millis();
  for (int a=0; a<2; a++) {
    if (a>1) { 
      delay(80); 
    }
    while((millis()-timeTracker)<120) {
      bottomRow();
    }
    timeTracker=millis();
    while((millis()-timeTracker)<120) {
      secondRow();
    }
    timeTracker=millis();
    while((millis()-timeTracker)<120) {
      thirdRow();
    }
    timeTracker=millis();
    while((millis()-timeTracker)<120) {
      topRow();
    }
    timeTracker=millis();
    while((millis()-timeTracker)<120) {
      stars();
    }
    timeTracker=millis();
    while((millis()-timeTracker)<120) {
      topRow();
    }
    timeTracker=millis();
    while((millis()-timeTracker)<120) {
      thirdRow();
    }
    timeTracker=millis();
    while((millis()-timeTracker)<120) {
      secondRow();
    }
    timeTracker=millis();
    while((millis()-timeTracker)<120) {
      bottomRow();
    }
  }
}

void enterSleep(void) {
  sleep_enable();
  sleep_cpu();
}

void setup() {

  adc_disable(); // ADC uses ~320uA
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);


  pinMode(0, INPUT);
  pinMode(1, INPUT);
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);

  upModeOne();
  delay(500);
  upModeTwo();
  delay(500);
  upDownModeOne();
  delay(500);
  upModeThree();
  delay(500);
}

void loop() {
  enterSleep();
}



