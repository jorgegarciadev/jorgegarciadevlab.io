function RGB(r,g,b){
    return 'rgb(' + Math.round(r) + ',' + Math.round(g) + ',' + Math.round(b) + ')';
}

var frequency = 0.3;
for (var i = 0; i < 21; i = i + 0.25) {
   red   = Math.sin(frequency*i + 0) * 127 + 128;
   green = Math.sin(frequency*i + 2) * 127 + 128;
   blue  = Math.sin(frequency*i + 4) * 127 + 128;

   var html = $('#page-content').html();
   $('#page-content').html(html + '<div class="color"></div>');
   // $('#page-content').children().last().after('<div class="color"></div>');
   $('#page-content').children().last().css('background-color', RGB(red, green, blue));
}
$('#page-content').children().last().after('<div class="color blanco"></div><div class="color negro"></div></div><div class="color rojo"></div></div><div class="color verde"></div></div><div class="color azul"></div><div class="color amarillo"></div>');



$(document).ready(function(){
  width=$('.color').width();
  $('.color').height(width);
});

$('.color').click(function(){
  color = $(this).css('background-color');
  console.log(color);
  var rgb = color.match(/\d+/g);
  console.log(rgb)
  $.get('rgb?r='+rgb[0]+'&g='+rgb[1]+'&b='+rgb[2], function(response){
    console.log(response)
  });
});