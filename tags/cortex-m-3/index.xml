<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Cortex M 3 on Jorge García</title>
    <link>https://jorgegarciadev.gitlab.io/tags/cortex-m-3/index.xml</link>
    <description>Recent content in Cortex M 3 on Jorge García</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>es-ES</language>
    <copyright>Jorge García. 2014 - 2018</copyright>
    <atom:link href="https://jorgegarciadev.gitlab.io/tags/cortex-m-3/index.xml" rel="self" type="application/rss+xml" />
    
      
          <item>
            <title>Mis primeros pasos con STM32 (I): Standart Peripheral Library</title>
            <link>https://jorgegarciadev.gitlab.io/post/mis_primeros_pasos_con_stm32_i/</link>
            <pubDate>Tue, 30 Jan 2018 05:16:39 +0100</pubDate>
            
            <guid>https://jorgegarciadev.gitlab.io/post/mis_primeros_pasos_con_stm32_i/</guid>
            <description>&lt;p&gt;Tras algunos años experimentando con los microcontroladores de Atmel y con los ESP de Espressif voy a probar cosas nuevas. He empezadon con los STM32 de STMicroelectronics, en concreto un STM32103C8T6. Se trata de un microcontrolador ARM Cortex-M3, sus principales características son:&lt;/p&gt;

&lt;table&gt;
&lt;thead&gt;
&lt;tr&gt;
&lt;th&gt;Características&lt;/th&gt;
&lt;th&gt;&lt;/th&gt;
&lt;/tr&gt;
&lt;/thead&gt;

&lt;tbody&gt;
&lt;tr&gt;
&lt;td&gt;Frecuencia&lt;/td&gt;
&lt;td&gt;72 MHZ&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;Flash&lt;/td&gt;
&lt;td&gt;64 kB (Hasta 128 kB)&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;RAM&lt;/td&gt;
&lt;td&gt;20 kB SRAM&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;ADC&lt;/td&gt;
&lt;td&gt;2 x 12-bit, 0 to 3,6 V&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;GPIO&lt;/td&gt;
&lt;td&gt;80 (5 V tolerant)&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;Debug&lt;/td&gt;
&lt;td&gt;SWD &amp;amp; JTAG&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;Timers&lt;/td&gt;
&lt;td&gt;7, 16-bit up to 4 IC/OC/PWM&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;Interfaces&lt;/td&gt;
&lt;td&gt;I2C, USART, SPI, CAN, USB 2.0&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;Otros&lt;/td&gt;
&lt;td&gt;7-Channel DMA, 16-bit motor controller, 2 watchdog timers, RTC&lt;/td&gt;
&lt;/tr&gt;
&lt;/tbody&gt;
&lt;/table&gt;

&lt;p&gt;Éste es el primer artículo de una serie dedicados a estos microcontroladores y diferentes plataformas. Para aprender esta familia de controladores he comprado una placa &lt;a href=&#34;http://wiki.stm32duino.com/index.php?title=STM32_Smart_V2.0&#34; target=&#34;_blank&#34;&gt;STM32 Smart V2.0&lt;/a&gt;, y la plataforma con la que voy a comenzar es la &lt;a href=&#34;http://www.st.com/content/st_com/en/products/embedded-software/mcus-embedded-software/stm32-embedded-software/stm32-standard-peripheral-libraries/stsw-stm32054.html&#34; target=&#34;_blank&#34;&gt;Standard Peripheral Library de ST&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;&lt;a id=&#34;lightbox&#34; href=&#34;https://jorgegarciadev.gitlab.io/images/stm32smartv2.jpg&#34; target=&#34;_blank&#34;&gt;&lt;img class=&#34;thumb&#34; src=&#34;https://jorgegarciadev.gitlab.io/images/stm32smartv2.jpg&#34;&gt;&lt;/a&gt;&lt;/p&gt;

&lt;p&gt;La Standard Peripheral Library está escrita en ANSI-C e incluye los &lt;em&gt;drivers&lt;/em&gt; y el API (rutinas, estructuras de datos y macros) para manejar todos los periféricos del microcotrolador. También incluye un sistema de detección de fallos en ejecución para facilitar la depuración. Incluye una colección de ejemplos que cubren todo el API y sus interfaces. En el &lt;a href=&#34;https://github.com/jorgegarciadev/stm32f103c8t6-spl-example&#34; target=&#34;_blank&#34;&gt;repositorio&lt;/a&gt; se incluye el manual de la SPL.&lt;/p&gt;

&lt;p&gt;Para empezar me he decidido por el &lt;em&gt;Hello World&lt;/em&gt; de los microcontroladores: Hacer parpadear un LED. La placa STM32 Smart V2.0 tiene un LED conectado al pin GPIOC13, y es el que utilizo en el ejemplo.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;cpp&#34;&gt;#include &amp;lt;stm32f10x.h&amp;gt;
#include &amp;lt;stm32f10x_gpio.h&amp;gt;
#include &amp;lt;stm32f10x_rcc.h&amp;gt;


void Delay(vu32 nCount)
{
  for(; nCount != 0; nCount--); // nCount / Clock speed = seconds
}

// GPIO structure for port initialization
GPIO_InitTypeDef GPIO_InitStructure;

int main(void) {
    // enable clock on APB2
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,  ENABLE);

    // configure port PC13 for driving an LED
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   // highest speed
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;    // output push-pull mode
    GPIO_Init(GPIOC, &amp;GPIO_InitStructure);             // initialize port

    // main loop
    while(1) {
        GPIO_SetBits(GPIOC, GPIO_Pin_13);    // turn the LED on
        Delay(0x17D7840); // delay 500 ms

        GPIO_ResetBits(GPIOC, GPIO_Pin_13);  // turn the LED off
        Delay(0x17D7840); // delay 500 ms
    }
}&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;El &lt;a href=&#34;https://github.com/jorgegarciadev/stm32f103c8t6-spl-example&#34; target=&#34;_blank&#34;&gt;repositorio&lt;/a&gt; incluye dos linker scripts, uno general para la familia STM32 y otro específico para STM32103C8T6. También incluye un &lt;samp&gt;Makefile&lt;/samp&gt;, es muy básico aunque puede usarse para cualquier proyecto ya que compila todos los drivers. El &lt;samp&gt;Makefile&lt;/samp&gt; tiene la opción upload, pensada para ser usado con &lt;a href=&#34;https://github.com/texane/stlink&#34; target=&#34;_blank&#34;&gt;&lt;samp&gt;stlink&lt;/samp&gt; de texane&lt;/a&gt;.
Para instala stlink necesitamos &lt;samp&gt;cmake&lt;/samp&gt;, &lt;samp&gt;build-essentials&lt;/samp&gt; y &lt;samp&gt;libusb-1.0.0-dev&lt;/samp&gt;. Una tengamos esos paquetes clonamos el repositorio, compilamos e instalamos:&lt;/p&gt;

&lt;p&gt;&lt;code&gt;$ git clone &lt;a href=&#34;https://github.com/texane/stlink.git&#34; target=&#34;_blank&#34;&gt;https://github.com/texane/stlink.git&lt;/a&gt;
$ cd stlink
stlink$ make release&lt;/code&gt;
$ cd build/Release
stlink/build/Release$ sudo make install&lt;/code&gt;&lt;/p&gt;

&lt;p&gt;Esto instalará el los herramientas en &lt;samp&gt;/usr/local/bin&lt;/samp&gt; y añadirá las reglas udev para el St-Link en &lt;samp&gt;etc/udev/rules.d&lt;/samp&gt;. Por último hay que comprar que se ha creado un grupo &lt;samp&gt;stlink&lt;/samp&gt;, y de no sear así crearlo, y añadir en el al usuario que va a utilizar el dispositivo:&lt;/p&gt;

&lt;p&gt;&lt;code&gt;$ sudo groupadd stlink
$ sudo usermod -aG stlink $(whoami)&lt;/code&gt;&lt;/p&gt;

&lt;p&gt;Para que los cambios surtan efecto, tanto las nuevas reglas udev como los cambios al usuario, recomiendo reiniciar el equipo.&lt;/p&gt;

&lt;p&gt;Para conectar el programador a la placa:&lt;/p&gt;

&lt;p&gt;&lt;a id=&#34;lightbox&#34; href=&#34;https://jorgegarciadev.gitlab.io/images/JTAG-conexion.png&#34; target=&#34;_blank&#34;&gt;&lt;img class=&#34;thumb&#34; src=&#34;https://jorgegarciadev.gitlab.io/images/JTAG-conexion.png&#34;&gt;&lt;/a&gt;&lt;/p&gt;

&lt;p&gt;En el caso de que la placa esté siendo alimentada por el USB no es necesario conectar &lt;samp&gt;vcc&lt;/samp&gt;. Ahora, para cargar el firmware al STM32 utilizaremos el comando:&lt;/p&gt;

&lt;p&gt;&lt;code&gt;$ make upload&lt;/code&gt;&lt;/p&gt;

&lt;p&gt;En el caso de que todo haya salido bien el LED que se encuentrar al lado de los pulsadores parpadeará.&lt;/p&gt;
</description>
          </item>
      
    
  </channel>
</rss>
